package multiFileLog

import (
	"math"
	"github.com/apsdehal/go-logger"
	"os"
)

type MultiFileLogger struct {
	files map[int][]*logger.Logger

}

func NewMultiFileLogger() (*MultiFileLogger){
	m := MultiFileLogger{}

	m.files = make(map[int][]*logger.Logger)

	m.files[INFO] = []*logger.Logger{}
	m.files[NOTICE] = []*logger.Logger{}
	m.files[WARNING] = []*logger.Logger{}
	m.files[ERROR] = []*logger.Logger{}
	m.files[CRITICAL] = []*logger.Logger{}
	m.files[FATAL] = []*logger.Logger{}
	m.files[DEBUG] = []*logger.Logger{}

	return &m
}


func (l *MultiFileLogger) SetDefaultLogFile(filePath string, name string) (error){
	return l.SetLogFile(filePath, name, INFO | NOTICE | WARNING | ERROR | CRITICAL | FATAL | DEBUG)
}

func (l *MultiFileLogger) SetLogFile(filePath string, name string, logLevel int) (error) {
	_, err := os.Open(filePath)

	var file *os.File

	if err != nil {
		//There was an error, the file couln'd be found. Create it!
		file, err = os.Create(filePath)
	} else {
		//No error, the file exists, open it for appending.
		file, err = os.OpenFile(filePath, os.O_APPEND, os.ModeAppend)
	}

	if err != nil {
		return err
	}

	logObj, err := logger.New(name, 0, file)

	if err != nil {
		return err
	}

	for i := 0; i < 7; i++ {
		idx := int(math.Pow(2.0, float64(i)))
		if((idx & logLevel) == idx) {
			l.files[idx] = append(l.files[idx], logObj)
		}
	}

	return nil
}

func (l *MultiFileLogger) Critical (log string) {
	for _, l := range l.files[CRITICAL] {
		l.Critical(log)
	}
}

func (l *MultiFileLogger) Debug (log string) {
	for _, l := range l.files[DEBUG] {
		l.Debug(log)
	}
}

func (l *MultiFileLogger) Warning (log string) {
	for _, l := range l.files[WARNING] {
		l.Warning(log)
	}
}

func (l *MultiFileLogger) Notice (log string) {
	for _, l := range l.files[NOTICE] {
		l.Notice(log)
	}
}

func (l *MultiFileLogger) Info (log string) {
	for _, l := range l.files[INFO] {
		l.Info(log)
	}
}

func (l *MultiFileLogger) Fatal (log string) {
	for _, l := range l.files[FATAL] {
		l.Fatal(log)
	}
}

func (l *MultiFileLogger) Error (log string) {
	for _, l := range l.files[ERROR] {
		l.Error(log)
	}
}