package multiFileLog

type NullLogger struct {


}

func NewNullLogger() (*NullLogger){
	m := NullLogger{}

	return &m
}

func (l *NullLogger) Critical (log string) {

}

func (l *NullLogger) Debug (log string) {

}

func (l *NullLogger) Warning (log string) {

}

func (l *NullLogger) Notice (log string) {

}

func (l *NullLogger) Info (log string) {

}

func (l *NullLogger) Fatal (log string) {

}

func (l *NullLogger) Error (log string) {

}