package multiFileLog


type Logger interface {
	Critical(string)
	Debug(string)
	Warning(string)
	Notice(string)
	Info(string)
	Fatal(string)
	Error(string)
}

const (
	INFO = 1
	NOTICE = 2
	WARNING = 4
	ERROR = 8
	CRITICAL = 16
	FATAL = 32
	DEBUG = 64
)
